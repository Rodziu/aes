<?php
/**
 *  __    __ _     _     _       _       _     ___  __
 * / / /\ \ (_) __| |___(_) __ _| |_ __ (_)   / _ \/ /
 * \ \/  \/ / |/ _` |_  / |/ _` | | '_ \| |  / /_)/ /
 *  \  /\  /| | (_| |/ /| | (_| | | | | | |_/ ___/ /___
 *   \/  \/ |_|\__,_/___|_|\__,_|_|_| |_|_(_)/   \____/
 *
 */
namespace WidzialniPL;
/**
 * Class AesTest
 * @package WidzialniPL
 */
class AesTest extends \PHPUnit_Framework_TestCase{
	/**
	 * @var Aes
	 */
	var $aes;

	/**
	 *
	 */
	public function setUp(){
		parent::setUp();
		$this->aes = new Aes();
	}

	/**
	 *
	 */
	public function testEncrypt(){
		$encrypted = $this->aes->encrypt("test", "key");
		$this->assertArrayHasKey("value", $encrypted);
		$this->assertArrayHasKey("iv", $encrypted);
	}

	/**
	 * @depends testEncrypt
	 */
	public function testDecrypt(){
		$encrypted = $this->aes->encrypt("test", "key");
		$this->assertEquals('test', $this->aes->decrypt($encrypted['value'], 'key', $encrypted['iv']));
		print_r($encrypted);
		//check if encryption still works with value encrypted by old script
		$encrypted = ["value" => "U2tF8/0M42ktL3v6b7hZISQTIcoCH7D4dHSoK6ep+pA=", "iv" => "3ZUjn57myIErqkrYmLbhCkm65Xpwq63eg182w+GCEz4="];
		$this->assertEquals('test', $this->aes->decrypt($encrypted['value'], 'key', $encrypted['iv']));
	}
}
