<?php
/**
 *  __    __ _     _     _       _       _     ___  __
 * / / /\ \ (_) __| |___(_) __ _| |_ __ (_)   / _ \/ /
 * \ \/  \/ / |/ _` |_  / |/ _` | | '_ \| |  / /_)/ /
 *  \  /\  /| | (_| |/ /| | (_| | | | | | |_/ ___/ /___
 *   \/  \/ |_|\__,_/___|_|\__,_|_|_| |_|_(_)/   \____/
 *
 */
namespace WidzialniPL;
/**
 * AES encryption/decryption class
 * uses CBC mode with 256bit keys
 *
 * @author Rodziu <mateusz.rohde@widzialni.pl>
 * @package WidzialniPL
 * @version 1.0
 */
class Aes{
	/**
	 * encrypt given $value using given $key
	 *
	 * @param string $value
	 * @param string $key
	 *
	 * @return array:string - value and iv, both base64 encoded
	 */
	public function encrypt($value, $key){
		$iv = $this->genIV();
		return [
			'value' => base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->hashKey($key), $value, MCRYPT_MODE_CBC, $iv)),
			'iv'    => base64_encode($iv)
		];
	}

	/**
	 * decrypt $value using given $key and initialization vector used for encryption
	 *
	 * @param string $value
	 * @param string $key
	 * @param string $iv
	 *
	 * @return string
	 */
	public function decrypt($value, $key, $iv){
		return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->hashKey($key), base64_decode($value), MCRYPT_MODE_CBC, base64_decode($iv)), "\0..\32");
	}

	/**
	 * create an hash for our secret key
	 *
	 * @param string $key
	 *
	 * @return string
	 */
	private function hashKey($key){
		return hash('sha256', $key, true);
	}

	/**
	 * generate initialization vector (IV) for CBC mode AES
	 * @return string
	 */
	private function genIV(){
		srand((double)microtime() * 1000000);
		return mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_RAND);
	}
}